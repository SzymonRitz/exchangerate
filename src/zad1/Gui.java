package zad1;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;

public class Gui extends JFrame {

	private JPanel contentPane;
	private JTextField countryField;
	private JTextField cityField;
	private Service service;
	private String defaultWeather;
	private Double defaultRate1;
	private Double defaultRate2;
	private JTextPane infoPane;
	private JTextField rateField;
	private JButton enterCity;
	private String weather;
	private String rate;

	
	public Gui(Service s) {
		service = s;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 674, 473);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		splitPane.setLeftComponent(panel);

		countryField = new JTextField();
		countryField.setColumns(10);

		JLabel lblNewLabel = new JLabel("Podaj państwo");
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);

		JLabel lblNewLabel_1 = new JLabel("Podaj miasto");

		cityField = new JTextField();
		cityField.setColumns(10);

		JButton enterCountry = new JButton("Wstaw");
		enterCountry.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String country = countryField.getText();
				service = new Service(country);
				splitPane.setRightComponent(new SwingFXWebView("https://pl.wikipedia.org/wiki/"+service.getCountry()));
				
				getWarningString();
			}
		});

		enterCity = new JButton("Wstaw");
		enterCity.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				// TODO Auto-generated method stub
				String city = cityField.getText();
				weather = service.getWeather(city);
				weather += "\n Kurs złotego wobec waluty tego państwa: ";
				weather += service.getNBPRate();
				Double temp;
				Double maxTemp;
				Double minTemp;

				Pattern pm = Pattern.compile("[0-9]{3}?.[0-9]{2}?");
				Matcher mm = pm.matcher(weather);

				mm.find();
				temp = new Double(mm.group());
				mm.find();
				maxTemp = new Double(mm.group());
				mm.find();
				minTemp = new Double(mm.group());
				
				weather = weather.replaceAll(".temp.:[0-9]{3}?.[0-9]{2}?", "Temperatura: " + (temp-273.15));
				weather = weather.replaceAll(".pressure.:", "Ciśnienie: ");
				weather = weather.replaceAll(".temp_max.:[0-9]{3}?.[0-9]{2}?", "Maksymalna temperatura: " + (maxTemp-273.15));
				weather = weather.replaceAll(".temp_min.:[0-9]{3}?.[0-9]{2}?", "Minimalna temperatura: " + (minTemp-273.15));
				weather = weather.replaceAll(".description.:", "Opis: ");
				weather = weather.replaceAll(".main.:", "Głównie: ");
				weather = weather.replaceAll(".deg.:", "Kierunek wiatru: ");
				weather = weather.replaceAll(".speed.:", "Prędkość wiatru: ");
				weather = weather.replaceAll(".country.:", "Państwo: ");
				weather = weather.replaceAll(".name.:", "nazwa: ");
				weather = weather.replaceAll("\"", " ");
				
				infoPane.setText(weather);
			}
		});
		
	    infoPane = new JTextPane();
		infoPane.setEditable(false);
		
		JLabel lblNewLabel_2 = new JLabel("Podaj walute przeliczeniową");
		
		rateField = new JTextField();
		rateField.setColumns(10);
		
		JButton enterRate = new JButton("Wstaw");
		enterRate.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				rate = "\nKurs podanej waluty wynosi: ";
				rate += service.getRateFor(rateField.getText())+"";
				infoPane.setText(weather + rate);
			}
		});
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(infoPane, GroupLayout.DEFAULT_SIZE, 533, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
							.addGap(0))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblNewLabel_2)
							.addContainerGap(409, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblNewLabel_1)
							.addContainerGap(482, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(rateField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
								.addComponent(cityField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
								.addComponent(countryField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(enterCountry)
									.addContainerGap())
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(enterCity)
									.addContainerGap())
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(enterRate)
									.addContainerGap())))))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(countryField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(enterCountry))
					.addGap(11)
					.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(cityField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(enterCity))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(rateField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(enterRate))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(infoPane, GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
					.addContainerGap())
		);
		panel.setLayout(gl_panel);

		splitPane.setRightComponent(new SwingFXWebView("https://wikipedia.org/wiki/"+service.getCountry()));

	}
	
	public void defaultWeather(String weatherJson) {
		defaultWeather = weatherJson;
	}
	public void defaultRate(Double rate1, Double rate2) {
		defaultRate1 = rate1;
		defaultRate2 = rate2;
	}
	
	public void prepareDefaultData(){
		String info;
		StringBuilder sb = new StringBuilder();
		sb.append(defaultWeather)
		.append("\n\nKurs waluty: ")
		.append(defaultRate1)
		.append("\n\nKurs wobec tej waluty: ")
		.append(defaultRate2);
		info = sb.toString();
		Double temp;
		Double maxTemp;
		Double minTemp;
		
		Pattern pm = Pattern.compile("[0-9]{3}?.[0-9]{2}?");
		Matcher mm = pm.matcher(info);

		mm.find();
		temp = new Double(mm.group());
		mm.find();
		maxTemp = new Double(mm.group());
		mm.find();
		minTemp = new Double(mm.group());
		
		info = info.replaceAll(".temp.:[0-9]{3}?.[0-9]{2}?", "Temperatura: " + (temp-273.15));
		info = info.replaceAll(".pressure.:", "Ciśnienie: ");
		info = info.replaceAll(".temp_max.:[0-9]{3}?.[0-9]{2}?", "Maksymalna temperatura: " + (maxTemp-273.15));
		info = info.replaceAll(".temp_min.:[0-9]{3}?.[0-9]{2}?", "Minimalna temperatura: " + (minTemp-273.15));
		info = info.replaceAll(".description.:", "Opis: ");
		info = info.replaceAll(".main.:", "Głównie: ");
		info = info.replaceAll(".deg.:", "Kierunek wiatru: ");
		info = info.replaceAll(".speed.:", "Prędkość wiatru: ");
		info = info.replaceAll(".country.:", "Państwo: ");
		info = info.replaceAll(".name.:", "nazwa: ");
		info = info.replaceAll("\"", " ");
	
		infoPane.setText(info);
	}
}
