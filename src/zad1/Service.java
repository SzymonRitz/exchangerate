/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

import com.chilkatsoft.CkHtmlToXml;

import sun.awt.dnd.SunDragSourceContextPeer;
import sun.security.jca.GetInstance.Instance;

public class Service {

	private HashMap<String, Locale> locales = new HashMap<String, Locale>();

	private String country;

	public Service(String string) {
		// TODO Auto-generated constructor stub
		setCountry(string);
		LoadData ld = new LoadData(this);
		ld.load();
	}

	public String getWeather(String string) {

		// System.out.println(c.getCurrencyCode());
		BufferedReader reader = null;
		StringBuilder sb = new StringBuilder();
		try {
			// System.out.println(countriesISO.get(country));
			locales.keySet().forEach(e -> System.out.println(e));
			System.out.println(string);
			System.out.println();
			URL url = new URL("http://api.openweathermap.org/data/2.5/weather?q=" + string + "," + locales.get(country).getCountry() 
					+ "&APPID=8073f9fcfe99ff0535e2013667413509");
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			JSONObject ob = new JSONObject(buffer.toString());

			Pattern pm = Pattern.compile(".[a-z-_]+.:[0-9]+.[0-9]+");
			Matcher mm = pm.matcher(ob.get("main").toString());
			while (mm.find()) {

				sb.append(mm.group()).append("\n");
			}
			Pattern pw = Pattern.compile("\"[a-zA-Z ]+\":\"[a-zA-Z ]+\"");
			Matcher mw = pw.matcher(ob.get("weather").toString());
			while (mw.find()) {
				sb.append(mw.group()).append("\n");
			}
			Pattern pwi = Pattern.compile(".[a-z]+.:[0-9]+.?[0-9]+");
			Matcher mwi = pwi.matcher(ob.get("wind").toString());
			while (mwi.find()) {
				sb.append(mwi.group()).append("\n");
			}

			Pattern ps = Pattern.compile(".country.:.[A-Z]+.");
			Matcher ms = ps.matcher(ob.get("sys").toString());
			while (ms.find()) {
				sb.append(ms.group()).append("\n");
			}
			Pattern pc = Pattern.compile("\"[a-z]*\":\"[A-Za-z]+\"");
			Matcher mc = pc.matcher(buffer.toString());
			while (mc.find()) {
				sb.append(mc.group()).append("\n");
			}

			return sb.toString();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block

		} catch (IOException e) {
			// TODO Auto-generated catch block

		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block

				}
		}
		return null;
	}

	public Double getRateFor(String string) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		String rate = null;

		try {
			URL url = new URL("http://api.fixer.io/latest?base=" + string);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];

			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			JSONObject json = new JSONObject(buffer.toString());
			String pattern = "." + Currency.getInstance(locales.get(country)).getSymbol(Locale.ENGLISH)
					+ ".:[0-9]+.[0-9]+";
			Pattern pw = Pattern.compile(pattern);
			Matcher mw = pw.matcher(json.get("rates").toString());

			while (mw.find()) {
				rate = mw.group().substring(6);
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
				}
		}
		return new Double(rate);
	}

	public Double getNBPRate() {
		// TODO Auto-generated method stub
		URL url = null;
		String rate = null;
		BufferedReader reader = null;
		String sURL = null;
		StringBuilder sb = new StringBuilder();
		String rates = null;
		try {
			url = new URL("http://www.nbp.pl/kursy/kursya.html");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
		}
		try {
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);
			sURL = buffer.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String pattern = "<td.class=.bgt[0-9] right.>.*";
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(sURL);
		while (m.find()) {
			sb.append(m.group());
			sb.append("\n");
		}

		rates = sb.toString();
		rates = rates.replaceAll("<td.class=.bgt[0-9] right.>", "");
		rates = rates.replaceAll("</td>", "");

		pattern = "1 " + Currency.getInstance(locales.get(country)).getSymbol(Locale.ENGLISH) + "\n.*";
		p = Pattern.compile(pattern);
		m = p.matcher(rates);
		while (m.find()) {
			rate = m.group().substring(6).replace(",", ".");
		}
		Double toReturne;

		try {
			toReturne = new Double(rate);
		} catch (NullPointerException ne) {
			return 1.0;
		}

		return toReturne;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public HashMap<String, Locale> getLocales() {
		return locales;
	}

	public void setLocales(HashMap<String, Locale> locales) {
		this.locales = locales;
	}

}