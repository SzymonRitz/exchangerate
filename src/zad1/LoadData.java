package zad1;

import java.util.Locale;


public class LoadData {

	private Service service;
	private Locale[] l = Locale.getAvailableLocales();

	public LoadData(Service service) {
		// TODO Auto-generated constructor stub
		this.service = service;

	}
	public void load() {

		for (int i = 0; i < l.length; i++) {
			service.getLocales().put(l[i].getDisplayCountry(Locale.ENGLISH), l[i]);
		}

	}

}
