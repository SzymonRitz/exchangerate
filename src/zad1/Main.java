/**
 *
 *  @author Ritz Szymon S12910
 *
 */

package zad1;


public class Main {
  public static void main(String[] args) {
    Service s = new Service("Poland");
    String weatherJson = s.getWeather("Warsaw");
    Double rate1 = s.getRateFor("USD");
    Double rate2 = s.getNBPRate();
    // ...
    // część uruchamiająca GUI
    Gui frame = new Gui(s);
    frame.defaultWeather(weatherJson);
    frame.defaultRate(rate1,rate2);
    frame.prepareDefaultData();
	frame.setVisible(true);
	frame.setResizable(true);
	frame.setSize(720, 480);
  }
}
