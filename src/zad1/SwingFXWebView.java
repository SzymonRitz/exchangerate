package zad1;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.JobAttributes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.sun.javafx.application.PlatformImpl;
  
/** 
 * SwingFXWebView 
 */  
public class SwingFXWebView extends JPanel {  
     
    private Stage stage;  
    private WebView browser;  
    private JFXPanel jfxPanel;  
    private WebEngine webEngine;  
    private String url;
  
    public SwingFXWebView(String url){  
        initComponents();  
        this.url = url;
    }  

    private void initComponents(){ 
         
        jfxPanel = new JFXPanel();
        
        createScene(); 

        setLayout(new BorderLayout());  
        add(jfxPanel);  
    }     

    private void createScene() {  
        PlatformImpl.startup(new Runnable() {  
            @Override
            public void run() {  
                 
                stage = new Stage();  
                 
                stage.setTitle("Hello Java FX");  
                stage.setResizable(true);  
   
                StackPane root = new StackPane();
                root.autosize();
                
                Scene scene = new Scene(root,jfxPanel.getHeight(),jfxPanel.getWidth());
                stage.setScene(scene);  
                
                browser = new WebView();
                webEngine = browser.getEngine();
                webEngine.load(url);
                
                ObservableList<Node> children = root.getChildren();
                children.add(browser);                     
               
                jfxPanel.setScene(scene);  
            }  
        });  
    }
}